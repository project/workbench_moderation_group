<?php

namespace Drupal\workbench_moderation_group\Plugin\GroupContentEnabler;

use Drupal\group\Entity\GroupInterface;
use Drupal\group\Plugin\GroupContentEnablerBase;

/**
 * Provides a content enabler for moderation state transition.
 *
 * @GroupContentEnabler(
 *   id = "workbench_moderation_group",
 *   label = @Translation("Workbench moderation for Group"),
 *   description = @Translation("Adds Moderation State Transition to groups."),
 *   entity_type_id = "moderation_state_transition",
 *   entity_access = TRUE,
 *   reference_label = @Translation("Title"),
 *   reference_description = @Translation("The title of the Moderation State Transition to add to the group")
 * )
 */
class GroupModerationStateTransition extends GroupContentEnablerBase {

  /**
   * {@inheritdoc}
   */
  protected function getTargetEntityPermissions() {
    $transition_permissions = new \Drupal\workbench_moderation_group\Permissions();
    $permissions = $transition_permissions->transitionPermissions();
    return $permissions;
  }

}
