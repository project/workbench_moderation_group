<?php

namespace Drupal\workbench_moderation_group;

use Drupal\Core\Routing\UrlGeneratorTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workbench_moderation\Entity\ModerationState;
use Drupal\workbench_moderation\Entity\ModerationStateTransition;

/**
 * Defines a class for dynamic permissions based on transitions.
 */
class Permissions {

  use StringTranslationTrait;
  use UrlGeneratorTrait;

  /**
   * Returns an array of transition permissions.
   *
   * @return array
   *   The transition permissions.
   */
  public function transitionPermissions() {
    // @todo write a test for this.
    $perms = [];
    /* @var \Drupal\workbench_moderation\ModerationStateInterface[] $states */
    $states = ModerationState::loadMultiple();
    /* @var \Drupal\workbench_moderation\ModerationStateTransitionInterface $transition */
    foreach (ModerationStateTransition::loadMultiple() as $id => $transition) {
      $title = 'Use the '.$transition->label().' transition';
      $description = 'Move content from '.$states[$transition->getFromState()]->label().' state to '.$states[$transition->getToState()]->label().' state.';
      $perms['use ' . $id . ' transition'] = [
        'title' => $title,
        'description' => $description,
      ];
    }

    return $perms;
  }

}
